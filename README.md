# gpsLogger

A simple GPS tracker with custom NMEA sentence parsing, storage to an SD card, and capable of running on battery voltages as low as 3.0V.

<img src="hardware/exports/model_front.png" height="300px"><img src="hardware/exports/model_back.png" height="300px">

## Table of Contents
- [gpsLogger](#gpslogger)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [About The Project](#about-the-project)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Ordering and Manufacturing](#ordering-and-manufacturing)
    - [Installation](#installation)
      - [Firmware](#firmware)
      - [Software](#software)
  - [Usage](#usage)
    - [Data Capture](#data-capture)
    - [Visualizer](#visualizer)
  - [Design Notes](#design-notes)
  - [Contributing](#contributing)

## Background
This coming summer (Summer 2021) I will be moving out to California for a summer internship but I will not be bringing a car with me. Instead, I will be bringing an electric longboard with me. The electric longboard that I have has a battery that is larger than the allowable limit for and flying and does not have an (easily) removeable battery. This means that I will be shipping my lonboard to California in a hard plastic case. 

While packing, I found a GPS module I had lying around and realized that, because I will already have a large battery in the case that, a couple extra small batteries for a GPS logger wouldn't hurt. I could use my knowledge of microcontrollers and low-power embedded systems to design a small system that would calculate the current position from a GPS module and log it to an SD card for future visualization. 

## About The Project
The device I have designed and created is a small form-factor GPS logger based on an Adafruit GPS breakout board for localization, an ATtiny microcontroller for data processing, a microSD card reader for expandable storage, and a single cell battery for power. Once all the data has been captured to the microSD card, the *.csv file can be loaded into the Jupyter notebook provided to clean up the data and create an interactive map in an *.html file. 

Once assembled, the device should continuously record data as long as the supply voltage (usually a battery) stays between 3.0v and 4.3v, and there is enough storage for around 100 bytes for every sample desired. This means a 128GB card should be able to store roughly 40.5+ years of time, location, altitude, and speed data points. 

What makes this project different is I have created everything from data capture to visualization. This means that everything should be a seamless and high-performance experience. I am not using a bloated GPS parser library that handles every type of NMEA sentence. Instead, I chose to make a very modular and well-documented library that can be easily adapted to support all types of sentences and storage limitations. 

## Getting Started
There are a couple of different parts to this project. The first is the hardware that runs the data data capture program which requires the PCB that has been designed. Once the PCB has been manufactured, the `gpslogger.ino` Arduino sketch needs to be uploaded to the device. Then you are ready to capture data. 

- Looking for how to order your own? Check out the section on [Ordering and Manufacturing](#ordering-and-manufacturing).
- Looking for how to upload the code to the device? Check out the section on [Firmware Installation](#firmware).
- Looking for how to install the visualization requirements? Check out the section on [Software Installation](#software)
- Looking for how to capture data? Check out the section on [Data Capture](#data-capture)
- Looking for how to run the visualization software? Check out the section on the [Visualizer](#visualizer)

### Prerequisites
I am assuming you have some very basic knowledge of microcontrollers, communication protocols, electrical wiring, and computer usage.

### Ordering and Manufacturing
For information on how to order the PCB, check out the [ordering_info](/hardware/exports/ordering_info.png) screenshot of the parameters I selected while ordering these PCB's from JLCPCB. Once the PCB's have arrived, follow the schematic to solder all the components and attach the GPS and battery to the PCB. 

### Installation

#### Firmware
To program the device, you can upload the `gpslogger.ino` file to the ATtiny3216 using the [Arduino IDE](https://www.arduino.cc/en/software) and [MegaTinyCore](https://github.com/SpenceKonde/megaTinyCore) by connecting your programmer to the UPDI header on the PCB. If you need help programming the ATtiny, check out my other project, [updiProgrammer](https://gitlab.com/Nathaniel.Belles/updiprogrammer) which goes into detail about how to use a simple USB to UART adapter and a couple passive components to upload Arduino sketches to the ATtiny. There are of course many ways to upload an Arduino sketch to an ATtiny, feel free to use whatever platform you are most comfortable using, the sketch is not specific to any particular programmer. 

#### Software
There are a couple of different ways to get the visualization part of the project up and running. If you already have a way to run a Python 3 Jupyter Notebook, feel free to use that, otherwise there are free, public, online ways to run Jupyter Notebooks like [Google Colab](https://colab.research.google.com/). If you prefer to run the visualization code as a python script, you can do that as well. Simply copy all the sections of code from the `visualizer.ipynb` file into your own python script and run that. 

There are a couple of external modules required listed below. If you do not have any of these already installed, you can typically install them by typing `pip3 install module` in a terminal (where 'module' is replaced with the module to be installed). 
- [folium](https://pypi.org/project/folium/)
- [pytz](https://pypi.org/project/pytz/)
- [timezonefinder](https://pypi.org/project/timezonefinder/)

## Usage
This project has two different aspects, the data capture side and the visualization side. Check out the respective sections below for more information on how to use the project as a data capture device or as a data visualizer. 

### Data Capture
The hardware device can capture GPS coordinates and heading/speed at regular intervals and store them to an SD card for later processing. It can record data indefinitely, assuming you have a large enough SD card and a large enough battery. I was able to test this device running for about 5 consecutive days on a 36,000mAh battery with almost no battery drain, recording to a 128GB microSD card producing a 55MB file (although your mileage may vary by GPS signal strength). 

To begin capturing data, ensure the firmware has been flashed to the ATtiny, a microSD card installed, and a power source (battery) connected to the battery terminals of the PCB. When the device is recording data you should see the LED indicator blinking for a short period at approximately 1 second intervals, assuming a GPS lock (indicated by a red LED on the GPS module). 

| Indicator             | Meaning                          |
| --------------------- | -------------------------------- |
| 2.5Hz 50% duty cycle  | microSD card not initialized     |
| 1Hz ~1-10% duty cycle | logging data to the microSD card |

To stop capturing data, unplug the microSD card or disconnect power from the battery terminals. Based on the method I used for writing to the microSD card it should minimize the chances of the data becoming corrupt. Once you're done capturing data, check out the visualization part of this project. 

### Visualizer
Once you have some data to visualize, you can use the `visualizer.ipynb` script to process the data and turn it into a fully interactive visualization. It takes in a `*.csv` file containing timestamp, coordinates, heading, and speed data points and turns it into an `*.html` file ready for use with any browser. 

<img src="software/visualization.png" height="300px">

To use the visualizer, start by selecting the `*.csv` file you want to pull data from and put it in the second code block of the `visulaizer.ipynb` file. Then choose the range and frequency of the data along with specific fields that you want to use from the csv file in the same code block. This code block will run and return the number of rows produced from filtering the data. The next code block prints out the first couple of rows and last couple of rows to make sure the section of data selected is correct. The fourth code block converts the timestamp in each sample to the corresponding location it was taken from. The next code block chooses the range and frequency of the data to be used in the visualization. This section also sets the GPS coordinates of the default start location of the visualization. This is useful for making the visualization look clean upon opening it. This block also sets the html string that is used in the popup when each data point is clicked on in the output visualization. For now it shows the index, date, altitude, speed and direction but can be changed to add any other information of interest. The next block is used to save the output file as an `*.html` file. 

The last code block is used for filtering out any unnecessary data from the input file. This data pops up whenever there is no GPS fix even though the gpsLogger is still trying to record the data. This block will generate a new `*.csv` file only containing the good data from the input file. 

I have provided a few examples of test data (without some of the beginning and ending data for security sake) that I used when testing this device in [this](software/tests/) folder. You can see how the visualizer code takes a csv file and converts it to an interactive html file. The final csv file I recorded when shipping my electric longboard from North Carolina to California and corresponding html file is provided as well. Feel free to check out the route it took!

## Design Notes
Here are a couple of notes on why I made the choices I made while designing this device. This section will most likely be placeholders while I'm testing certain features where I won't make the most optimal design decision (size, part-count, or efficiency) and instead make something easy to debug or test certain features. 

- Real time communication: There was a lot of thought that was put into whether or not to include some form of communication module that would allow for real-time visualization of the location of the device. All options that were found were either expensive, required a lot more development work, weren't reliable while traveling cross-country, or took up a lot more space. If, in the future, I need to use this project again, I might revisit this project and redesign it to include a communication device like cellular, WiFi or LoRa.
- GPS module vs. GPS breakout board: Because I already had a GPS breakout board lying around, I chose to use it as the main part of my design. I am aware that there are better, cheaper, more power efficient modules without all the extra breakout board components that I would much rather use. If I redesign this, I would like to use a raw GPS module and add the required supporting circuitry myself. In this current design it bypasses all the supporting circuitry onboard the breakout board to utilize the full voltage range of the GPS module (3.0v - 4.3v). 
- Battery voltage range: I am currently designing this for running on a pretty significant battery which means the battery voltage will not change much over time given how little current this device will consume. Technically, the GPS module is supposed to support the voltage range I will be running it on but just be cautious that it isn't guaranteed to run on the standard small battery voltage range. I would recommend doing some current draw calculations based on how long you are trying to run it for and determine exactly how much battery capacity you will need. 
- Signal strength: With the GPS module I used, I did have access to an external GPS antenna but I don't think it was good enough to provide enough signal strength throughout the duration of the trip from North Carolina to California. If you look at the final output [file](software/05.24.2021.html) from the main trip, you can see how it must have lost signal in Kernersville, North Carolina and then regained it for a short time in San Bernardino, California and then finally getting a good signal again outside of Los Angleles, California. I think it would be possible to have this perform better with a better antenna but there's not much that can overcome being put at the bottom of the pile of packages in a large metal box that is the shipping truck. As much as I would have wanted this to perform better, I think it showed that it works well when it does have a good signal (which is most other scenarios than this specific thing where it it stuck in a giant metal box for most of the useable recording time).
- The elephant in the room: Apple's AirTags. The Fall after I created this device, Apple released the AirTag which gave the functionality of tracking any object whenever an Apple device is nearby. The advantage of the AirTag is that anyone can buy it off-the-shelf, without needing to program it, it has a multi-year battery life, and the current location can be accessed in real time (assuming a nearby Apple device with internet, battery and a GPS lock). The downside of the AirTag is that it requires an Apple device to be nearby it at all times, it doesn't have any ability to log the data over time (you only get current location at this instant). 

## Contributing
Feel free to contribute to this project. I welcome you to open issues, make pull requests, or just fork it and add your own features!