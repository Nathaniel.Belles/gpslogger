#ifndef GPS_H
#define GPS_H

#include <Arduino.h>

class GPS
{
public:
    // Processing variables
    uint8_t nextIndex;
    String stringToParse;
    String nextSubstring;
    String stringRepresentation;
    void (*loggingFunction)(String);

    // Time
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;

    // Satellite data
    uint8_t fix;
    uint8_t satCount;

    // Location Data
    uint8_t lat_deg;
    float lat_min;
    int8_t lat_dir;
    uint8_t long_deg;
    float long_min;
    int8_t long_dir;
    float altitude;

    // Speed and Direction
    float speed;
    float direction;

    // Data status
    uint8_t status;

    // Input and output functions
    void setLoggingFunction(void (*)(String));
    void parseString(String toParse);
    String toString();

private:
    void getNextSubstring();
    void parseGPGGA();
    void parseGPRMC();
    void parseTimeSubstring();
    void parseLatitudeSubstring();
    void parseLongitudeSubstring();
    void parseFixSubstring();
    void parseSatCount();
    void parseAltitudeSubstring();
    void parseStatusSubstring();
    void parseSpeedSubstring();
    void parseDirectionSubstring();
    void parseDateSubstring();
};

#endif // GPS_H