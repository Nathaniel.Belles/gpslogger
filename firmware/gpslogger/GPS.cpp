#include "GPS.h"

String GPSRepresentationString = "Year,Month,Day,Hour,Minute,Second,Fix,Satellite Count,Latitude Degrees,Latitude Minutes,Latitude Direction,Longitude Degrees,Longitude Minutes,Longitude Direction,Altitude,Speed,Direction,Status";
const char comma = ',';

void GPS::setLoggingFunction(void (*functionToUse)(String))
{
    loggingFunction = functionToUse;
};

void GPS::parseString(String toParse)
{
    // Store the string for parsing
    stringToParse = toParse;

    if (stringToParse.startsWith("$GPGGA,"))
    {
        // Serial.print("GPGGA String Found: " + stringToParse);

        parseGPGGA();
    }
    else if (stringToParse.startsWith("$GPRMC,"))
    {
        // Serial.print("GPRMC String Found: " + stringToParse);

        parseGPRMC();

        // This is where storing data will happen
        (*loggingFunction)(toString());
    }
};

String GPS::toString()
{
    // "Year,Month,Day,Hour,Minute,Second,Fix,Satellite Count,Latitude Degrees,Latitude Minutes,Latitude Direction,Longitude Degrees,Longitude Minutes,Longitude Direction,Altitude,Speed,Direction,Status";
    stringRepresentation = "";
    stringRepresentation += year;
    stringRepresentation += comma;
    stringRepresentation += month;
    stringRepresentation += comma;
    stringRepresentation += day;
    stringRepresentation += comma;
    stringRepresentation += hour;
    stringRepresentation += comma;
    stringRepresentation += minute;
    stringRepresentation += comma;
    stringRepresentation += second;
    stringRepresentation += comma;
    stringRepresentation += fix;
    stringRepresentation += comma;
    stringRepresentation += satCount;
    stringRepresentation += comma;
    stringRepresentation += lat_deg;
    stringRepresentation += comma;
    stringRepresentation += String(lat_min, 4);
    stringRepresentation += comma;
    stringRepresentation += lat_dir;
    stringRepresentation += comma;
    stringRepresentation += long_deg;
    stringRepresentation += comma;
    stringRepresentation += String(long_min, 4);
    stringRepresentation += comma;
    stringRepresentation += long_dir;
    stringRepresentation += comma;
    stringRepresentation += String(altitude, 4);
    stringRepresentation += comma;
    stringRepresentation += String(speed, 4);
    stringRepresentation += comma;
    stringRepresentation += String(direction, 4);
    stringRepresentation += comma;
    stringRepresentation += status;

    //    Serial.print("toString: ");
    //    Serial.println(stringRepresentation);

    return stringRepresentation;
}

void GPS::getNextSubstring()
{
    nextIndex = stringToParse.indexOf(',');
    nextSubstring = stringToParse.substring(0, nextIndex);
    stringToParse.remove(0, nextIndex + 1);
};

void GPS::parseGPGGA()
{
    getNextSubstring(); // $GPGGAA
    // Serial.print("GPGGA Data: " + stringToParse);

    parseTimeSubstring(); // hhmmss.sss
    // Serial.print("Current Time: ");
    // Serial.print(hour);
    // Serial.print(":");
    // Serial.print(minute);
    // Serial.print(":");
    // Serial.println(second);

    parseLatitudeSubstring(); // llll.llll,a
    // Serial.print("Current GPS Latitude: ");
    // Serial.print(lat_deg);
    // Serial.print("d ");
    // Serial.print(lat_min, 4);
    // Serial.println("m");
    // Serial.print("Latitude Orientation: ");
    // Serial.println(lat_dir);

    parseLongitudeSubstring(); // yyyyy.yyyy,a
    // Serial.print("Current GPS Longitude: ");
    // Serial.print(long_deg);
    // Serial.print("d ");
    // Serial.print(long_min, 4);
    // Serial.println("m");
    // Serial.print("Longitude Orientation: ");
    // Serial.println(long_dir);

    parseFixSubstring(); // x
    // Serial.print("GPS Fix: ");
    // Serial.println(fix);

    parseSatCount(); // xx
    // Serial.print("Satellite Count: ");
    // Serial.println(satCount);

    getNextSubstring(); // x.x
    // Serial.println("HDOP: " + nextSubstring);

    parseAltitudeSubstring(); // x.x
    // Serial.print("Altitude: ");
    // Serial.println(altitude);

    // There are more parameters in the string to be printed
    // but they aren't relevant to the current project.
};

void GPS::parseGPRMC()
{
    getNextSubstring(); // $GPGGAA
    // Serial.print("GPRMC Data: " + stringToParse);

    parseTimeSubstring(); // hhmmss.sss
    // Serial.print("Current Time: ");
    // Serial.print(hour);
    // Serial.print(":");
    // Serial.print(minute);
    // Serial.print(":");
    // Serial.println(second);

    parseStatusSubstring(); // A
    // Serial.print("Status: ");
    // Serial.println(status);

    parseLatitudeSubstring(); // llll.llll,a
    // Serial.print("Current GPS Latitude: ");
    // Serial.print(lat_deg);
    // Serial.print("d ");
    // Serial.print(lat_min, 4);
    // Serial.println("m");
    // Serial.print("Latitude Orientation: ");
    // Serial.println(lat_dir);

    parseLongitudeSubstring(); // yyyyy.yyyy,a
    // Serial.print("Current GPS Longitude: ");
    // Serial.print(long_deg);
    // Serial.print("d ");
    // Serial.print(long_min, 4);
    // Serial.println("m");
    // Serial.print("Longitude Orientation: ");
    // Serial.println(long_dir);

    parseSpeedSubstring(); // x.x
    // Serial.print("Speed: ");
    // Serial.println(speed);

    parseDirectionSubstring(); // x.x
    // Serial.print("Direction: ");
    // Serial.println(direction);

    parseDateSubstring(); // ddmmyy
    // Serial.print("Current Date: ");
    // Serial.print(day);
    // Serial.print(" ");
    // Serial.print(month);
    // Serial.print(" ");
    // Serial.println(year);
};

void GPS::parseTimeSubstring()
{
    getNextSubstring();
    // Serial.println("Time: " + nextSubstring);

    nextIndex = nextSubstring.indexOf('.');
    nextSubstring.remove(nextIndex);
    // Serial.println("Time cropped: " + nextSubstring);

    hour = nextSubstring.substring(0, 2).toInt();
    minute = nextSubstring.substring(2, 4).toInt();
    second = nextSubstring.substring(4, 6).toInt();
    // Serial.print("Current Time: ");
    // Serial.print(hour);
    // Serial.print(":");
    // Serial.print(minute);
    // Serial.print(":");
    // Serial.println(second);
};

void GPS::parseLatitudeSubstring()
{
    getNextSubstring();
    // Serial.println("GPGGA Latitude String: " + nextSubstring);

    lat_deg = nextSubstring.substring(0, 2).toInt();
    lat_min = nextSubstring.substring(2).toFloat();
    // Serial.print("Current GPS Latitude: ");
    // Serial.print(lat_deg);
    // Serial.print("d ");
    // Serial.print(lat_min, 4);
    // Serial.println("m");

    getNextSubstring();
    // Serial.println("GPGGA Latitude Orientation: " + nextSubstring);

    if (nextSubstring.startsWith("N"))
    {
        lat_dir = 1;
    }
    else if (nextSubstring.startsWith("S"))
    {
        lat_dir = -1;
    }
    else
    {
        lat_dir = 0;
    }
    // Serial.print("Latitude Orientation: ");
    // Serial.println(lat_dir);
};

void GPS::parseLongitudeSubstring()
{
    getNextSubstring();
    // Serial.println("GPGGA Longitude String: " + nextSubstring);

    long_deg = nextSubstring.substring(0, 3).toInt();
    long_min = nextSubstring.substring(3).toFloat();
    // Serial.print("Current GPS Longitude: ");
    // Serial.print(long_deg);
    // Serial.print("d ");
    // Serial.print(long_min, 4);
    // Serial.println("m");

    getNextSubstring();
    // Serial.println("Longitude Orientation: " + nextSubstring);
    if (nextSubstring.startsWith("E"))
    {
        long_dir = 1;
    }
    else if (nextSubstring.startsWith("W"))
    {
        long_dir = -1;
    }
    else
    {
        long_dir = 0;
    }
    // Serial.print("Longitude Orientation: ");
    // Serial.println(long_dir);
};

void GPS::parseFixSubstring()
{
    getNextSubstring();
    // Serial.println("GPS Fix: " + nextSubstring);

    fix = nextSubstring.toInt();
    // Serial.print("GPS Fix: ");
    // Serial.println(fix);
};

void GPS::parseSatCount()
{
    getNextSubstring();
    // Serial.println("Satellite Count: " + nextSubstring);

    satCount = nextSubstring.toInt();
    // Serial.print("Satellite Count: ");
    // Serial.println(satCount);
};

void GPS::parseAltitudeSubstring()
{
    getNextSubstring();
    // Serial.println("Altitude: " + nextSubstring);

    altitude = nextSubstring.toFloat();
    // Serial.print("Altitude: ");
    // Serial.println(altitude);
};

void GPS::parseStatusSubstring()
{
    getNextSubstring(); // A
    // Serial.println("Status: " + nextSubstring);

    if (nextSubstring.startsWith("A"))
    {
        status = 1;
    }
    else // if (nextSubstring.startsWith("V"))
    {
        status = 0;
    }
    // Serial.print("Status: ");
    // Serial.println(status);
};

void GPS::parseSpeedSubstring()
{
    getNextSubstring();
    // Serial.println("Speed: " + nextSubstring);

    speed = nextSubstring.toFloat();
    // Serial.print("Speed: ");
    // Serial.println(speed);
};

void GPS::parseDirectionSubstring()
{
    getNextSubstring();
    // Serial.println("Direction: " + nextSubstring);

    direction = nextSubstring.toFloat();
    // Serial.print("Direction: ");
    // Serial.println(direction);
};

void GPS::parseDateSubstring()
{
    getNextSubstring();
    // Serial.println("Date: " + nextSubstring);

    day = nextSubstring.substring(0, 2).toInt();
    month = nextSubstring.substring(2, 4).toInt();
    year = nextSubstring.substring(4, 6).toInt();
    // Serial.print("Current Date: ");
    // Serial.print(day);
    // Serial.print(" ");
    // Serial.print(month);
    // Serial.print(" ");
    // Serial.println(year);
};
