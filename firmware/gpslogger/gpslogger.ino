#include "GPS.h"
#include <SPI.h>
#include <SD.h>
#include <avr/sleep.h>

#define BATTERY_CUTOFF 3000 // in milliVolts

extern String GPSRepresentationString;
String buffer = "";
GPS currentGPSData;

void setup()
{
    // Setup pin for logging activity
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    // Setup Serial interfaces
    Serial.begin(9600);

    // Send configuration to the GPS module
    Serial.println("$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"); // GPGGA, GPRMC
    Serial.println("$PMTK220,1000*1F");                                  // Output frequency: 1Hz

    // Set logging function
    currentGPSData.setLoggingFunction(logToSD);

    // Initialize the SD card interface
    if (!SD.begin())
    {
        // Stop program
        while (1)
        {
            // // Indicate error by blinking LED
            digitalWrite(LED_BUILTIN, HIGH);
            delay(200);
            digitalWrite(LED_BUILTIN, LOW);
            delay(200);
        }
    }

    // Log header
    logToSD(GPSRepresentationString);

    // Configure sleep mode
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);

    // Enable sleep mode, wait for command to sleep
    sleep_enable();
};

void loop()
{
    // Find string start
    int stringStart = buffer.indexOf("$");

    // Find string end
    int stringEnd = buffer.indexOf('\n', stringStart);

    // Check if complete string was found
    if (stringStart > -1 && stringEnd > -1)
    {
        // Send useful part of string to GPSData object
        currentGPSData.parseString(buffer.substring(stringStart, stringEnd));

        // Remove parsed string from buffer
        buffer.remove(0, stringEnd);
    };

    checkBatteryVoltage();
};

// Called in-between each loop for storing to the buffer
void serialEvent()
{
    while (Serial.available())
    {
        // Copy all the data to the buffer
        char c = Serial.read();
        buffer += c;
    }
};

// Called after complete sentence has been parsed
void logToSD(String data)
{
    // Turn on activity LED
    digitalWrite(LED_BUILTIN, HIGH);

    // Open object to write to
    File dataFile = SD.open("datalog.txt", FILE_WRITE);
    if (dataFile)
    {
        // Add String to data file and close
        dataFile.println(data);
        dataFile.close();
    }
    else
    {
        // Error handling
    }

    // Turn off activity LED
    digitalWrite(LED_BUILTIN, LOW);
};

// An ATtiny-specific function for checking the battery voltage
void checkBatteryVoltage()
{
// Check if compiling with MegaTinyCore
#ifdef MEGATINYCORE_SERIES
    // Set analog voltage reference
    analogReference(VDD);

    VREF.CTRLA = VREF_ADC0REFSEL_1V5_gc;
    uint16_t reading = analogRead(ADC_INTREF);
    reading = analogRead(ADC_INTREF);
    uint32_t intermediate = 1023UL * 1500;
    reading = intermediate / reading;
    if (reading <= BATTERY_CUTOFF)
    {
        // Shutdown
        sleep_cpu();
    };
#else
#error "Make sure you are compiling using MegaTinyCore. If not, remove 'checkBatteryVoltage()' from your program."
#endif
}